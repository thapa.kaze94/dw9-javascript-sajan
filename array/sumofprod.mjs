// sum of product prices
// [{name:"earphone",price:1200},
//              {name:"charge",price:2500},
//              {name:"battery",price:4000} ] ==> [1200,2500,4000] use map method and then use reduce method

let product=[{name:"earphone",price:1200},
             {name:"charge",price:2500},
             {name:"battery",price:4000} ]
let ar1=product.map((value,i) => {
           return value.price

})
console.log(ar1);
let amount=ar1.reduce((p,c) => {
    return p+c
})
console.log("Total amount is", amount);
//  print whose length is max
let list=["sajan","ramcharn","hari"]
