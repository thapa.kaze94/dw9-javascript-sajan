// let input=[1,2,1,["a","b"],["a","b"]]
// let ar1=input.map((value,i)=>{
//         return JSON.stringify(value)
//     })
// let set=[... new Set(ar1)]
// let setParse=set.map((value,i)=>{
//     return JSON.parse(value)
// })
// console.log(setParse);

let fun = (a) => {
    let ar1 = a.map((value, i) => {
        return JSON.stringify(value);
    })
    let set = [...new Set(ar1)];
    console.log(set);
    let setParse = set.map((value, i) => {
        return JSON.parse(value);
    })
    return setParse;
}

let output = fun([1, 2 , 1, ['a', 'b']]);

console.log(output);
